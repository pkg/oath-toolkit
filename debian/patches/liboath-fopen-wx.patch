From 3235a52f6b87cd1c5da6508f421ac261f5e33a70 Mon Sep 17 00:00:00 2001
From: Simon Josefsson <simon@josefsson.org>
Date: Tue, 1 Oct 2024 10:12:18 +0200
Subject: [PATCH 2/8] Improve liboath usersfile write handling.

Thanks to Fabian Vogt of SUSE for finding and reporting the problem.

Open *.lock and *.new file in exclusive mode to treat any existing
files as an error, including if it is a symbolic link.

Modify so that if opening the lock file fails, it returns
OATH_FILE_LOCK_ERROR instead of OATH_FILE_CREATE_ERROR so that the
latter is reserved to mean error to create the target file.

Fix bug that causes stale *.lock file to be kept around on failure to
open *.new file.
---
 liboath/usersfile.c | 8 +++++---
 1 file changed, 5 insertions(+), 3 deletions(-)

diff --git a/liboath/usersfile.c b/liboath/usersfile.c
index 3b139d1..68268a2 100644
--- a/liboath/usersfile.c
+++ b/liboath/usersfile.c
@@ -329,11 +329,11 @@ update_usersfile (const char *usersfile,
     if (lockfile == NULL || ((size_t) l) != strlen (usersfile) + 5)
       return OATH_PRINTF_ERROR;
 
-    lockfh = fopen (lockfile, "w");
+    lockfh = fopen (lockfile, "wx");
     if (!lockfh)
       {
 	free (lockfile);
-	return OATH_FILE_CREATE_ERROR;
+	return OATH_FILE_LOCK_ERROR;
       }
   }
 
@@ -365,15 +365,17 @@ update_usersfile (const char *usersfile,
     if (newfilename == NULL || ((size_t) l) != strlen (usersfile) + 4)
       {
 	fclose (lockfh);
+	unlink (lockfile);
 	free (lockfile);
 	return OATH_PRINTF_ERROR;
       }
 
-    outfh = fopen (newfilename, "w");
+    outfh = fopen (newfilename, "wx");
     if (!outfh)
       {
 	free (newfilename);
 	fclose (lockfh);
+	unlink (lockfile);
 	free (lockfile);
 	return OATH_FILE_CREATE_ERROR;
       }
-- 
2.46.0

